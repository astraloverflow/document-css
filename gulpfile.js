// Load Gulp
const gulp = require('gulp')

// Load Gulp Plugins
const plugin = {
  postcss: require('gulp-postcss'),
  stylelint: require('gulp-stylelint')
}

// Load PostCSS Plugins
const postcss = {
  import: require('postcss-import'),
  cssnext: require('postcss-cssnext'),
  clean: require('postcss-clean'),
  reporter: require('postcss-reporter')
}

// Load BrowserSync (for livereloading w/ device sync)
const browserSync = require('browser-sync')

gulp.task('default', ['build'])

gulp.task('serve', ['default'], () => {
  // Rebuild styles on change
  gulp.watch(['source/**'], ['build'])

  return browserSync({
    // Options found here: https://browsersync.io/docs/options
    // Open control panel in browser
    open: 'ui',
    // Delay before reloading (milliseconds)
    reloadDelay: 2000,
    // Custom prefix for console.log messages sent by BrowserSync
    logPrefix: 'BrowserSync',
    // Run a development server
    server: {
      // Folder w/ our built files
      baseDir: './docs/demos/',
      // Set to true for directory listings
      directory: true
    }
  })
})

gulp.task('test', () =>
  gulp.src(['source/*.css', '!source/__*.css'])
    // Lint & style check CSS code
    // Config located in .stylelint.yml
    .pipe(plugin.stylelint({
      // Output results to CLI
      reporters: [
        {
          formatter: 'string',
          console: true
        }
      ]
    }))
)

gulp.task('build', ['test'], () =>
  gulp.src(['source/*.css', '!source/_*.css'])
      .pipe(plugin.postcss([
        postcss.import(), // Resolve @import statements
        postcss.cssnext(), // Pre-processes new CSS features (see cssnext.io)
        postcss.clean({inline: false}), // Minifies CSS
        postcss.reporter() // Output errors messages & other info
      ]))
    .pipe(gulp.dest('./'))
    .pipe(gulp.dest('./docs/demos/css/'))
    // Reload/Inject into the browser
    .pipe(browserSync.stream())
)
