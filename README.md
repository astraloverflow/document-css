document.css
============

[![License][license-img]][license-url]
[![Version][version-img]][version-url]
[![Maintenance](https://img.shields.io/maintenance/yes/2017.svg)][graph-url]

> Semantic Styling for Simple HTML Documents

- [Quick Start](#quick-start)
- [Usage](#usage)
- [Themes](#themes)

## Quick Start

### RawGit CDN
*Easiest Option*

1. Add the following code to your HTML document's `<head>`

```html
<link rel="stylesheet" href="https://cdn.rawgit.com/astraloverflow/document.css/1.0.0-beta.1/document.css" />
```

### Node/NPM
*Only available if you have Node LTS 6 or higher installed*

1. Inside your project directory run `npm install --save astraloverflow/document.css#1.0.0-beta.1`
2. Add the following code to your HTML document's `<head>`

```html
<link rel="stylesheet" href="node_modules/document.css/document.css" />
```

## Usage

To Do

## Themes

If the default styling is not to your taste you can change the appearance of your web page with the use of a theme. A theme in the context of document.css is an overall look or aesthetic (fonts, colors, alignment, etc). Only one theme can be used at a time, otherwise document.css will exhibit strange behavior.

### Default

This is the default theme, simply follow the instructions in the [Quick Start](#quick-start) section above to use this theme.

**[Live Demo](https://astraloverflow.github.io/document.css/demos/default.html)**

![Screenshot of the default theme](https://placehold.it/400x400?text=Coming+Soon)

### Formal

If you are using RawGit CDN then add the following code to your HTML document's `<head>`

```html
<link rel="stylesheet" href="https://cdn.rawgit.com/astraloverflow/document.css/1.0.0-beta.1/theme-formal.css" />
```

If you are using Node/NPM then add the following code to your HTML document's `<head>`

```html
<link rel="stylesheet" href="node_modules/document.css/theme-formal.css" />
```

**[Live Demo](https://astraloverflow.github.io/document.css/demos/formal.html)**

![Screenshot of the formal theme](https://placehold.it/400x400?text=Coming+Soon)

### Modern

If you are using RawGit CDN then add the following code to your HTML document's `<head>`

```html
<link rel="stylesheet" href="https://cdn.rawgit.com/astraloverflow/document.css/1.0.0-beta.1/theme-modern.css" />
```

If you are using Node/NPM then add the following code to your HTML document's `<head>`

```html
<link rel="stylesheet" href="node_modules/document.css/theme-modern.css" />
```

**[Live Demo](https://astraloverflow.github.io/document.css/demos/modern.html)**

![Screenshot of the modern theme](https://placehold.it/400x400?text=Coming+Soon)

### Yotsuba-b

If you are using RawGit CDN then add the following code to your HTML document's `<head>`

```html
<link rel="stylesheet" href="https://cdn.rawgit.com/astraloverflow/document.css/1.0.0-beta.1/theme-yotsuba-b.css" />
```

If you are using Node/NPM then add the following code to your HTML document's `<head>`

```html
<link rel="stylesheet" href="node_modules/document.css/theme-yotsuba-b.css" />
```

**[Live Demo](https://astraloverflow.github.io/document.css/demos/yotsuba-b.html)**

![Screenshot of the yotsuba-b theme](https://placehold.it/400x400?text=Coming+Soon)


[license-url]: https://github.com/astraloverflow/document.css/blob/master/LICENSE
[license-img]: https://img.shields.io/github/license/astraloverflow/document.css.svg

[version-url]: https://github.com/astraloverflow/document.css/releases
[version-img]: https://img.shields.io/github/release/astraloverflow/document.css.svg

[graph-url]: https://github.com/astraloverflow/document.css/graphs/contributors
